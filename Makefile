DESCRIPTION=

all: list.task

# Include general config
include $(wildcard $(CURDIR)/tasks/config/*.mk)
include $(wildcard $(CURDIR)/tasks/*.mk)

DESCRIPTION+="list.task: Available Tasks"
list.task:
	@$(ECHO) "Tasks List: (APP_ENVIRONMENT=$(APP_ENVIRONMENT))" \
	&& (for TASK in $(DESCRIPTION); do echo "-" "$$TASK"; done) \
	| sort -t: --key=1,1

.PHONY: help.%
help.%:
	@(for LINE in $(HELP_$*); do $(ECHO) $$LINE; done)