example-%: NAMESPACE=decking

.PHONY: example-%.image
example-%.image: docker.image.build.example-%
	$(NOP)

.PHONY: example-%.remove
example-%.remove: docker.container.remove.example-%
	$(NOP)

.PHONY: example-%.create
example-%.create: example-%.image docker.container.create.example-%
	$(NOP)

example-%.start: docker.container.start.example-%
	$(NOP)

example-%.stop: docker.container.stop.example-%
	$(NOP)


# Redis container
example-redis.%: NAME?=example-redis
example-redis.%: IMAGE?=$(NAMESPACE)/example-redis
example-redis.%: SOURCE?=https://github.com/dockerfile/redis.git
example-redis.%: OPTS?=
example-redis.%: CMD?=



# Mongodb container
example-mongodb.%: NAME?=example-mongodb
example-mongodb.%: IMAGE?=$(NAMESPACE)/example-mongodb
example-mongodb.%: SOURCE?=https://github.com/dockerfile/mongodb.git
example-mongodb.%: OPTS?=
example-mongodb.%: CMD?=
example-mongodb.create: CMD=/usr/bin/mongod --noprealloc --smallfiles --nojournal



# Api container
example-api.%: NAME?=example-api
example-api.%: IMAGE?=$(NAMESPACE)/example-api
example-api.%: SOURCE?="$(CURDIR)/src/api"
example-api.%: PORTS?=7777:7777
example-api.%: LINKS?=example-redis:redis example-mongodb:db
example-api.%: OPTS?=
example-api.%: CMD=--verbose

example-api.create: \
	example-redis.create\
	example-mongodb.create\
	example-api.remove

example-api.start: \
	example-redis.start \
	example-mongodb.start

example-api.stop: \
	example-redis.stop \
	example-mongodb.stop



# Admin container
example-admin.%: NAME?=example-admin
example-admin.%: IMAGE="$(NAMESPACE)/example-admin"
example-admin.%: SOURCE="$(CURDIR)/src/admin"
example-admin.%: PORTS?=8887:8887
example-admin.%: LINKS?=example-api:api
example-admin.%: OPTS?=
example-admin.%: CMD=--verbose

example-admin.create: \
	example-api.create \
	example-admin.remove

example-admin.start: \
	example-api.start



# Web container
example-web.%: NAME?=example-web
example-web.%: IMAGE="$(NAMESPACE)/example-web"
example-web.%: SOURCE="$(CURDIR)/src/web"
example-web.%: PORTS?=8888:8888
example-web.%: LINKS?=example-api:api
example-web.%: OPTS?=
example-web.%: CMD=--verbose


example-web.create: \
	example-api.create \
	example-web.remove

example-web.start: \
	example-api.start
