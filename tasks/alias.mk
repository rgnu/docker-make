.PHONY: ps
DESCRIPTION+="ps: Alias for docker.containers"
ps: docker.containers
	$(NOP)

.PHONY: ps.all
DESCRIPTION+="ps.all: Alias for docker.containers OPTS=-a"
ps.all: OPTS=-a
ps.all: docker.containers
	$(NOP)

.PHONY: images
DESCRIPTION+="images: Alias for docker.images"
images: docker.images
	$(NOP)

.PHONY: rmi.%
DESCRIPTION+="rmi.<NAME>: Alias for docker.image.remove"
rmi.%: docker.image.remove.%
	$(NOP)

.PHONY: rm.%
DESCRIPTION+="rm.<NAME>: Alias for docker.container.rm"
rm.%: docker.container.remove.%
	$(NOP)

.PHONY: exec.%
DESCRIPTION+="exec.<NAME> <CMD>: Alias for docker.container.exec"
exec.%: docker.container.exec.%
	$(NOP)

.PHONY: shell.%
DESCRIPTION+="shell.<NAME>: Shell"
shell.%: OPTS=-ti
shell.%: CMD=sh
shell.%: docker.container.exec.%
	$(NOP)

.PHONY: info.%
DESCRIPTION+="info.<NAME>: Get container info"
info.%: docker.container.info.%
	$(NOP)

.PHONY: ip.%
DESCRIPTION+="ip.<NAME>: Get container ip"
ip.%: OPTS=--format '$*: {{ .NetworkSettings.IPAddress }}'
ip.%: docker.container.info.%
	$(NOP)

.PHONY: logs.%
DESCRIPTION+="logs.<NAME>: Get container logs"
logs.%: OPTS?=--tail=100 -f
logs.%: docker.container.logs.%
	$(NOP)
