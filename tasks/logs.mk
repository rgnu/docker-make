# Global Tasks

.PHONY: logs-%.remove
logs-%.remove: docker.container.remove.logs-%
	$(NOP)

.PHONY: logs-%.create
logs-%.create: docker.container.create.logs-%
	$(NOP)

.PHONY: logs-%.start
logs-%.start: docker.container.start.logs-%
	$(NOP)

.PHONY: logs-%.stop
logs-%.stop: docker.container.stop.logs-%
	$(NOP)

.PHONY: logs-%.ip
logs-%.ip: OPTS=--format '{{ .NetworkSettings.IPAddress }}'
logs-%.ip: docker.container.info.logs-%
	$(NOP)


# Global Config
logs-%: NAMESPACE=olx-inc


# Logspout container
logs-logspout.%: NAME?=logs-logspout
logs-logspout.%: IMAGE?=progrium/logspout
logs-logspout.%: SOURCE?=
logs-logspout.%: VOLUMES?=/var/run/docker.sock:/tmp/docker.sock
logs-logspout.%: OPTS?=
logs-logspout.%: CMD?=


.PHONY: logs.tail
DESCRIPTION+="logs.tail: Tail logs"
logs.tail: IP?=$(shell $(MAKE) --no-print-directory logs-logspout.ip)
logs.tail: logs-logspout.create logs-logspout.start
	$(CURL) $(IP):8000/logs

.PHONY: logs.start
DESCRIPTION+="logs.start: Start logs environment"
logs.start: \
	logs-logspout.create \
	logs-logspout.start

.PHONY: logs.stop
DESCRIPTION+="logs.stop: Stop logs environment"
logs.stop: \
	logs-logspout.stop
